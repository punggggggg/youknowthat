package com.example.youknowthat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class last extends Activity implements OnClickListener {
	String showScore  = "";
	int highS=0;
	String c = "";
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_last);
		
		
		
		File infile = getBaseContext().getFileStreamPath("province.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				
				c = reader.readLine().trim();
				highS =Integer.parseInt(c);
				reader.close();
			} catch (Exception e) {
				//Do nothing
				 
			}
		}
		
		 
		
		
		
		TextView tv = (TextView)findViewById(R.id.tvS);
		TextView tvhs = (TextView)findViewById(R.id.tvHS);
		Bundle bundle = getIntent().getExtras();
		int ts = bundle.getInt("sendscore");
		String b = String.valueOf(ts);
		tv.setText(b); 
		
		if (highS<ts){
			highS=ts;
			
		}
		 c = String.valueOf(highS);
		tvhs.setText(c);
		save(c);
		
		Button back = (Button)findViewById(R.id.button1);
		back.setOnClickListener(this);


	}
	
	public void save(String c){
		try {
			FileOutputStream outfile = openFileOutput("province.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(c);
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
		
		
		
		
		
	}
	
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(outState != null){
			super.onSaveInstanceState(outState);
		}
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent goHome = new Intent(getApplicationContext(),MainActivity.class); 
		startActivity(goHome);
		
	}

}
	