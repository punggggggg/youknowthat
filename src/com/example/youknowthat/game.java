package com.example.youknowthat;







import java.util.Random;






import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

public class game extends Activity implements OnItemClickListener{
	Integer[] ar = { R.drawable.one, R.drawable.two,R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six,R.drawable.seven,R.drawable.eight,R.drawable.nine};
	int Click;
	int score=0;
	Integer rowbases=null,loops=0;
	ImageView imageView, firstView;
	String res  = "";
	TextView tvTimer;
	
	   @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_game);
	        
	        tvTimer = (TextView)findViewById(R.id.tvTimer);
	        
	        
	        CountDownTimer cdt = new CountDownTimer(60000, 50) {
	            public void onTick(long millisUntilFinished) {
	            	String strTime = String.format("%.1f" , (double)millisUntilFinished / 1000);
	                tvTimer.setText(String.valueOf(strTime));    
	            }

	            public void onFinish() {
	            	 
	            	 
	            	
	            	 Intent goEnd= new Intent(game.this,last.class);
	            	 goEnd.putExtra("sendscore", score);
	            	 startActivity(goEnd);
	            }
	            
	        
	        }.start();
	              
	        android.widget.GridView gridview = (android.widget.GridView) findViewById(R.id.gridview1);
	        gridview.setVerticalSpacing ( 5 );
	        gridview.setHorizontalSpacing ( 5 );
	        gridview.setAdapter(new ImageAdapter(this));
	        gridview.setOnItemClickListener(this); 
	        
	        rand();
	        
	   }
	   public void rand() {
			// TODO Auto-generated method stub
			Random random = new Random();
	    	rowbases=random.nextInt(10);
	    	 
	    	if (rowbases==0)
			{
	    		
	    		ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.oneone );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 		         
				
			}
			else if (rowbases==1)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.twotwo );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==2)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.threethree );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==3)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.fourfour );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==4)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.fivefive );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==5)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.sixsix );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==6)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.sevenseven );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==7)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.eighteight );
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			else if (rowbases==8)
			{
				ImageView image = ( ImageView )findViewById (R.id.imagepic2);
		         
		         image.setImageResource ( R.drawable.ninenine);
		          
		         image.setScaleType ( ScaleType.FIT_XY ); 	
			}
			
				
		}
	   
	   public class ImageAdapter extends BaseAdapter
		{
		private Context context;
		public ImageAdapter(Context c)
		{
		// TODO Auto-generated method stub
		context = c;
		}
		public int getCount() {
		
		// TODO Auto-generated method stub
		
		return ar.length;
		
		}
		
		 
		
		public Object getItem(int position) {
		
		// TODO Auto-generated method stub

		return position;
		
		}
		
		public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ImageView imageView;
			//imageView
			if (convertView == null) {
			imageView = new ImageView(context);
			imageView.setLayoutParams(new GridView.LayoutParams(130, 120));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(4, 4, 4, 4);
			} else {
				imageView = (ImageView) convertView;
					}
			imageView.setImageResource(ar[position]);
			return imageView;
			} 
			}
	   
	   
	    

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
		// TODO Auto-generated method stub
		
		 Click = position;
	       
	        if(Click==rowbases){
	        	
	        	score++;
	        }
	          
	        	rand();
	        	//TextView tv = (TextView)findViewById(R.id.textView1);
	       	
	           // tv.setText(res);
	        	
	            TextView tv2 = (TextView)findViewById(R.id.tvScore);
	        	res =  String.valueOf(score);
	            tv2.setText(res);
	        
 		}
		
		
	}


